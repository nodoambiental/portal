---
title: "Proyectos"
categories: ["R"]
tags: ["R Markdown"]
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(collapse = TRUE)
```

El _objetivo general_ es ayudar a reducir los impactos negativos de actividades humanas sobre el ambiente y la salud.

**Estrella Blanca** 

_Objetivos_

- Reducir la mortalidad de fauna silvestre en rutas y caminos.

- Difundir información sobre las funciones de la fauna silvestre que mantienen recursos útiles para el hombre.

_Fundamento_

La fauna silvestre ayuda en el mantenimiento de la salud del ambiente, mediante la regulación de plagas, la dispersión de semillas de especies de plantas útiles, los ciclos de nutrientes que mantienen la salud del suelo, entre otras funciones que ayudan a mantener recursos naturales que utilizamos. Sin embargo, actualmente la mortalidad de fauna silvestre por tráfico vehicular es un problema para la conservación de la fauna y en consecuencia, de los recursos útiles.

**Salud Alimentaria**

_Objetivos_

- Reducir el uso de venenos y su impacto sobre la salud humana y ambiental.

- Difundir información sobre salud y seguridad agro-alimentaria.

_Fundamento_

Los venenos se fabrican en general para atacar plagas u organismos que suelen reducir la producción agrícola. Sin embargo, la mayoría de estos venenos tienen impactos negativos, persistentes y muy difíciles de erradicar. Excepto en condiciones de exclusión (islas o zonas rodeadas de barreras geográficas), es casi imposible la erradicación de plagas. Esta situación ha generado el uso masivo de venenos, que tienen dos consecuencias indeseables: generan resistencia en las plagas, e incorporan sustancias venenosas en nuestros alimentos y desechos.
